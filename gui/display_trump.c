#include <stdio.h>
#include <stdlib.h>
#include <string.h>

///charge a file and display it (classic GUI)
void display_trump(char *fileName, int choice, int goal_score)
{
    FILE *file = fopen(fileName, "r");
    int nbSearchFound = 0;
    if (file == NULL)
    {
        fprintf(stderr, "Error opening file %s", fileName);
        return;
    }

    char c;

    while ((c = fgetc(file)) != EOF)
    {
        if (c == ',')
        {
            nbSearchFound++;
            if (nbSearchFound == choice)
            {
                printf("*");
            }
            else
            {
                printf(" ");
            }
        }
        else if (c == '$')
        {
            char tmp[3];
            sprintf(tmp, "%d", goal_score);
            printf("%s", tmp);
            int i;
            for (i = 0; i < 4 - strlen(tmp); i++)
            {
                printf(" ");
            }
        }
        else
        {
            printf("%c", c);
        }
    }
    fclose(file);
}