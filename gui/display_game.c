#include <stdio.h>
#include <stdlib.h>
#include "../settings.h"
#include <string.h>
#include "./gui.h"

///charge a file and display it (classic GUI)
void display_game(char *fileName, int choice, Player *player, int phase, int goal_score)
{
    system("clear");
    FILE *file = fopen(fileName, "r");
    int card_found = 0;
    int auction_found = 0;
    if (file == NULL)
    {
        fprintf(stderr, "Error opening file %s", fileName);
        return;
    }
    int c;

    while ((c = fgetc(file)) != EOF)
    {
        if (c == '$')
        {
            printf("%s", player[0].name);
        }
        else if (c == ';')
        {
            switch (auction_found)
            {
            case 0:
                printf("%s", player[2].contract.display);
                break;
            case 1:
                printf("%s", player[1].contract.display);
                break;
            case 2:
                printf("%s", player[3].contract.display);
                break;
            case 3:
                printf("%s", player[0].contract.display);
                break;
            }
            auction_found++;
        }
        else if (c == '@')
        {
            printf("%s", player[0].card[card_found].display);
            int i;
            for (i = 0; i < 5 - strlen(player[0].card[card_found].display); i++)
            {
                printf(" ");
            }
            card_found++;
        }
        else if (c == '^')
        {
            switch (phase)
            {
            case 1:
                display_auction("./gui/auction.txt", ',', choice);
                break;
            case 2:
                display_trump("./gui/trump.txt", choice, goal_score);
                break;
            }
        }
        else
        {
            printf("%c", c);
        }
    }
    fclose(file);
}
