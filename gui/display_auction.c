#include <stdio.h>
#include <stdlib.h>
#include <string.h>

///charge a file and display it (classic GUI)
void display_auction(char *fileName, char search, int choice)
{
    FILE *file = fopen(fileName, "r");
    int nbSearchFound = 0;
    if (file == NULL)
    {
        fprintf(stderr, "Error opening file %s", fileName);
        return;
    }

    char c;

    while ((c = fgetc(file)) != EOF)
    {
        if (c == search)
        {
            nbSearchFound++;
            if (nbSearchFound == choice)
            {
                printf("*");
            }
            else
            {
                printf(" ");
            }
        }
        else
        {
            printf("%c", c);
        }
    }
    fclose(file);
}