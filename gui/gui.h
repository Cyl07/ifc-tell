#ifndef GUI_H_INCLUDED
#define GUI_H_INCLUDED

#include "../settings.h"

void display(char *, char, int);
void display_auction(char *, char, int);
void display_trump(char *, int, int);
void display_new_game(char *, char, char *);
void display_game(char *, int, Player *, int, int);

#endif
