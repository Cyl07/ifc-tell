#include <stdio.h>
#include <stdlib.h>

///charge a file and display it (classic GUI)
void display_new_game(char *fileName, char search, char *name)
{

    system("clear");
    FILE *file = fopen(fileName, "r");
    if (file != NULL)
    {
        int c;

        while ((c = fgetc(file)) != EOF)
        {
            if (c == search)
            {
                printf("%s", name);
            }
            else
            {
                printf("%c", c);
            }
        }
        fclose(file);
    }
    else
    {
        printf("Error on fileName");
    }
}
