#include <stdio.h>
#include <stdlib.h>

///charge a file and display it (classic GUI)
void display(char *fileName, char search, int choice)
{

    system("clear");
    FILE *file = fopen(fileName, "r");
    int nbSearchFound = 0;
    if (file != NULL)
    {
        int c;

        while ((c = fgetc(file)) != EOF)
        {
            if (c == search)
            {
                nbSearchFound++;
                if (nbSearchFound == choice)
                {
                    printf("*");
                }
                else
                {
                    printf(" ");
                }
            }
            else
            {
                printf("%c", c);
            }
        }
        fclose(file);
    }
    else
    {
        printf("Error on fileName");
    }
}
