#include "../settings.h"

#ifndef FUNCTION_H_INCLUDE
#define FUNCTION_H_INCLUDE

void new_game(void);
void score(void);
void quit(void);
int keyCheck(int, int);
int write_name(char *, int);
void game(char *);
void initialize_deck(Card *);
void initialize_player(Card *, Player *, char *);
void shuffle(Card *);
void get_contract_display(Contract *);
int select_trump(int, int, int *);

#endif
