#include "../settings.h"
#include <stdio.h>
#include <string.h>

void initialize_player(Card *deck, Player *player, char *name)
{
    int i = 0, j = 0;
    strcpy(player[0].name, name);
    strcpy(player[1].name, "ouest\0");
    strcpy(player[2].name, "nord\0");
    strcpy(player[3].name, "est\0");

    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 3; j++)
        {
            player[i].card[j] = deck[i * 3 + j];
        }
    }
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 2; j++)
        {
            player[i].card[j + 3] = deck[i * 2 + j + 12];
        }
    }
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 3; j++)
        {
            player[i].card[j + 5] = deck[i * 3 + j + 20];
        }
    }
    /*for (i = 0; i < 4; i++)
    {
        printf("%s\n", player[i].name);
        for (j = 0; j < 8; j++)
        {
            printf("%s\n", player[i].card[j].display);
        }
    }*/
}