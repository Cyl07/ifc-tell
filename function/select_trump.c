#include <ncurses.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

int select_trump(int choice, int nb_choice, int *goal_score)
{
    int ch;
    bool score_lock = false;
    if (*goal_score == 500 || *goal_score == 250)
    {
        score_lock = true;
    }
    char ch_score[4];
    sprintf(ch_score, "%d", *goal_score);

    char str[2];

    initscr();
    raw();
    keypad(stdscr, TRUE);
    noecho();
    ch = getch();
    switch (ch)
    {
    case KEY_UP:
        if (choice > 1)
        {
            choice--;
        }
        break;
    case KEY_DOWN:
        if (choice < nb_choice)
        {
            choice++;
        }
        break;
    case 10:
        choice += 50;
        break;
    }
    if (!score_lock)
    {
        if ((ch <= 57 && ch >= 48) && strlen(ch_score) < 3)
        {
            str[0] = ch;
            strcat(ch_score, str);
            *goal_score = atoi(ch_score);
        }
        if (ch == 263 && strlen(ch_score) > 0)
        {
            ch_score[strlen(ch_score) - 1] = 0;
            *goal_score = atoi(ch_score);
        }
    }

    endwin();

    return choice;
}