#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include "functions.h"

int write_name(char *name, int choice)
{
    char ch;
    char str[2];

    initscr();
    raw();
    keypad(stdscr, TRUE);
    noecho();
    ch = getch();
    if (ch == 10)
    {
        choice += 50;
    }
    else if (((ch <= 90 && ch >= 65) || (ch <= 122 && ch >= 97) || ch == 32) && strlen(name) < 19)
    {
        strcpy(str, "\0");
        str[0] = ch;
        strncat(name, str, 1);
    }
    else if (ch == 7 && strlen(name) > 0)
    {
        name[strlen(name) - 1] = 0;
    }
    endwin();
    return choice;
}