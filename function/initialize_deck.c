#include "../settings.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void initialize_deck(Card *deck)
{
    int i = 0, j = 0;
    char car[3];
    for (i = 0; i < 4; i++)
    {
        for (j = 1; j < 9; j++)
        {
            deck[i * 8 + j - 1].color = i;
            strcpy(deck[i * 8 + j - 1].display, "\0");
            switch (j)
            {
            case 1:
                sprintf(car, "%d", 1);
                strcat(car, "\0");
                strcat(deck[i * 8 + j - 1].display, car);
                deck[i * 8 + j - 1].trump_value = 11;
                deck[i * 8 + j - 1].not_trump_value = 11;
                deck[i * 8 + j - 1].all_trump_value = 6;
                deck[i * 8 + j - 1].none_trump_value = 19;
                break;
            case 4:
                sprintf(car, "%d", 9);
                strcat(car, "\0");
                strcat(deck[i * 8 + j - 1].display, car);
                deck[i * 8 + j - 1].trump_value = 14;
                deck[i * 8 + j - 1].not_trump_value = 0;
                deck[i * 8 + j - 1].all_trump_value = 9;
                deck[i * 8 + j - 1].none_trump_value = 0;
                break;
            case 5:
                sprintf(car, "%d", 10);
                strcat(car, "\0");
                strcat(deck[i * 8 + j - 1].display, car);
                deck[i * 8 + j - 1].trump_value = 10;
                deck[i * 8 + j - 1].not_trump_value = 10;
                deck[i * 8 + j - 1].all_trump_value = 5;
                deck[i * 8 + j - 1].none_trump_value = 10;
                break;
            case 6:
                strcat(deck[i * 8 + j - 1].display, "J");
                deck[i * 8 + j - 1].trump_value = 20;
                deck[i * 8 + j - 1].not_trump_value = 2;
                deck[i * 8 + j - 1].all_trump_value = 14;
                deck[i * 8 + j - 1].none_trump_value = 2;
                break;
            case 7:
                strcat(deck[i * 8 + j - 1].display, "Q");
                deck[i * 8 + j - 1].trump_value = 3;
                deck[i * 8 + j - 1].not_trump_value = 3;
                deck[i * 8 + j - 1].all_trump_value = 1;
                deck[i * 8 + j - 1].none_trump_value = 3;
                break;
            case 8:
                strcat(deck[i * 8 + j - 1].display, "K");
                deck[i * 8 + j - 1].trump_value = 4;
                deck[i * 8 + j - 1].not_trump_value = 4;
                deck[i * 8 + j - 1].all_trump_value = 3;
                deck[i * 8 + j - 1].none_trump_value = 4;
                break;
            default:
                sprintf(car, "%d", j + 5);
                strcat(car, "\0");
                strcat(deck[i * 8 + j - 1].display, car);
                deck[i * 8 + j - 1].trump_value = 0;
                deck[i * 8 + j - 1].not_trump_value = 0;
                deck[i * 8 + j - 1].all_trump_value = 0;
                deck[i * 8 + j - 1].not_trump_value = 0;
                break;
            }
            switch (i)
            {
            case 0:
                strcat(deck[i * 8 + j - 1].display, "♠");
                break;
            case 1:
                strcat(deck[i * 8 + j - 1].display, "♣");
                break;
            case 2:
                strcat(deck[i * 8 + j - 1].display, "♥");
                break;
            case 3:
                strcat(deck[i * 8 + j - 1].display, "♦");
                break;
            }
            //printf("%s\n", deck[i * 8 + j - 1].display);
        }
    }
}