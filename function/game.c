#include "../settings.h"
#include "./functions.h"
#include "../gui/gui.h"
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

void game(char *name)
{
    Card deck[32];
    Player player[4];
    Contract contract;

    int opening_player = 2;
    int phase = 1;
    int pass_count = 0;
    contract_type best_contract = NONE;
    int best_goal_score = 0;
    int best_trump;
    int nb_contract_annouced = 0;

    contract_type type = NONE;
    int goal_score = 0;

    initialize_deck(deck);
    shuffle(deck);
    initialize_player(deck, player, name);

    int choice = 1;
    bool exit = false;
    while (!exit)
    {
        int i = opening_player, j;
        switch (phase)
        {
        case 1:
            for (j = 0; j < 4; j++)
            {
                contract.color = 0;
                contract.type = NONE;
                contract.goal_score = 0;
                get_contract_display(&contract);
                player[j].contract = contract;
            }
            do
            {
                if (i % 4 > 0)
                {
                    int nb_trump_card[4];
                    best_trump = -1;
                    for (j = 0; j < 4; j++)
                    {
                        nb_trump_card[j] = 0;
                    }
                    for (j = 0; j < 8; j++)
                    {
                        nb_trump_card[player[i % 4].card[j].color]++;
                    }
                    for (j = 0; j < 4; j++)
                    {
                        if (nb_trump_card[j] == 8)
                        {
                            if (best_contract < GENERAL)
                            {
                                best_trump = j;
                                type = GENERAL;
                                goal_score = 500;
                                best_contract = GENERAL;
                                best_goal_score = goal_score;
                            }
                        }
                        else if (nb_trump_card[j] >= 4)
                        {
                            if (best_contract <= BID && best_goal_score < 120)
                            {
                                best_trump = j;
                                type = BID;
                                goal_score = 120;
                                best_contract = BID;
                                best_goal_score = goal_score;
                            }
                        }
                        else if (nb_trump_card[j] == 3)
                        {
                            if (best_contract <= BID && best_goal_score < 80)
                            {
                                best_trump = j;
                                type = BID;
                                goal_score = 80;
                                best_contract = BID;
                                best_goal_score = goal_score;
                            }
                        }
                        if (best_trump == -1)
                        {
                            type = PASS;
                            goal_score = 0;
                        }
                    }
                    nb_contract_annouced++;
                }
                else
                {
                    choice = 1;
                    do
                    {
                        display_game("./gui/game.txt", choice, player, 1, 0);
                        choice = keyCheck(choice, 5);
                        if (choice > 50)
                        {
                            choice -= 50;
                            switch (choice)
                            {
                            case 1:
                                type = PASS;
                                goal_score = 0;
                                choice += 50;
                                break;
                            case 2:
                                if (best_contract <= BID)
                                {
                                    choice = 1;
                                    goal_score = best_goal_score;
                                    type = BID;
                                    do
                                    {
                                        display_game("./gui/game.txt", choice, player, 2, goal_score);
                                        choice = select_trump(choice, 7, &goal_score);
                                    } while (choice < 50);
                                    choice -= 50;
                                    switch (choice)
                                    {
                                    case 7:
                                        choice = 1;
                                        break;
                                    default:
                                        best_trump = choice - 1;
                                        best_goal_score = goal_score;
                                        choice += 50;
                                        break;
                                    }
                                }
                                break;
                            case 3:
                                if (best_contract < CAPOT)
                                {
                                    choice = 1;
                                    type = CAPOT;
                                    goal_score = 250;
                                    do
                                    {
                                        display_game("./gui/game.txt", choice, player, 2, goal_score);
                                        choice = select_trump(choice, 7, &goal_score);
                                    } while (choice < 50);
                                    choice -= 50;
                                    switch (choice)
                                    {
                                    case 7:
                                        choice = 1;
                                        break;
                                    default:
                                        best_trump = choice - 1;
                                        best_goal_score = goal_score;
                                        best_contract = CAPOT;
                                        choice += 50;
                                        break;
                                    }
                                }
                                break;
                            case 4:
                                if (best_contract < GENERAL)
                                {
                                    choice = 1;
                                    type = GENERAL;
                                    goal_score = 500;
                                    do
                                    {
                                        display_game("./gui/game.txt", choice, player, 2, goal_score);
                                        choice = select_trump(choice, 7, &goal_score);
                                    } while (choice < 50);
                                    choice -= 50;
                                    switch (choice)
                                    {
                                    case 7:
                                        choice = 1;
                                        break;
                                    default:
                                        best_trump = choice - 1;
                                        best_goal_score = goal_score;
                                        best_contract = GENERAL;
                                        choice += 50;
                                        break;
                                    }
                                }
                                break;
                            case 5:
                                if (best_contract < DOUBLE && best_contract >= 1)
                                {
                                    type = DOUBLE;
                                    goal_score = best_goal_score * 2;
                                }
                                break;
                            }
                            nb_contract_annouced++;
                        }
                    } while (choice < 50);
                }
                contract.color = best_trump;
                contract.type = type;
                contract.goal_score = goal_score;
                get_contract_display(&contract);
                player[i % 4].contract = contract;
                pass_count = 0;
                for (j = 0; j < 4; j++)
                {
                    if (player[j].contract.type == PASS)
                    {
                        pass_count++;
                    }
                }
                i++;
            } while (pass_count < 3 || nb_contract_annouced < 4);
            phase++;
            display_game("./gui/game.txt", choice, player, 1, 0);
            break;
        };
    }
}