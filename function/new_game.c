#include "../gui/gui.h"
#include "../function/functions.h"
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

void new_game(void)
{
    int choice = 1;
    char *name = malloc(sizeof(char) * 20);
    strcpy(name, "\0");
    bool exit = false;
    while (!exit)
    {
        display_new_game("./gui/new_game.txt", ',', name);
        choice = write_name(name, choice);
        if (choice > 50)
        {
            choice -= 50;
            exit = true;
            game(name);
            free(name);
        }
    }
}