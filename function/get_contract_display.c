#include "../settings.h"
#include <string.h>
#include <stdio.h>

void get_contract_display(Contract *contract)
{
    if (contract->type == PASS)
    {
        strcpy(contract->display, "Pass\0");
    }
    else if (contract->type == BID)
    {
        char tmp[3];
        sprintf(tmp, "%d", contract->goal_score);
        strcpy(contract->display, tmp);
    }
    else if (contract->type == GENERAL)
    {
        strcpy(contract->display, "General\0");
    }
    else if (contract->type == CAPOT)
    {
        strcpy(contract->display, "Capot\0");
    }
    else if (contract->type == NONE)
    {
        strcpy(contract->display, "None\0");
    }
    if (contract->type <= GENERAL && contract->type >= BID)
    {
        switch (contract->color)
        {
        case 0:
            strcat(contract->display, "♠");
            break;
        case 1:
            strcat(contract->display, "♣");
            break;
        case 2:
            strcat(contract->display, "♥");
            break;
        case 3:
            strcat(contract->display, "♦");
            break;
        case 4:
            strcat(contract->display, "♠ ♣ ♥ ♦");
            break;
        case 5:
            strcat(contract->display, "*");
            break;
        }
    }
}