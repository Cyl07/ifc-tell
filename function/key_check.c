#include <stdio.h>
#include <stdbool.h>
#include <ncurses.h>

///allow moves on the GUI
int keyCheck(int choice, int nbChoice)
{

    int ch;

    /* Curses Initialisations */
    initscr();
    raw();
    keypad(stdscr, TRUE);
    noecho();
    ch = getch();
    switch (ch)
    {
    case KEY_UP:
        if (choice > 1)
        {
            choice--;
        }
        break;
    case KEY_DOWN:
        if (choice < nbChoice)
        {
            choice++;
        }
        break;
    case 10:
        choice += 50;
        break;
    }

    endwin();

    return choice;
}
