#include "../settings.h"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

void shuffle(Card *deck)
{
    srand(time(NULL));
    Card tmp_card;
    int i = 0;

    for (i = 0; i < 1500; i++)
    {
        int pos = rand() % 32;
        tmp_card = deck[pos];
        deck[pos] = deck[i % 32];
        deck[i % 32] = tmp_card;
    }
}