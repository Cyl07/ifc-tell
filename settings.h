#ifndef SETTINGS_H
#define SETTINGS_H

typedef enum
{
    NONE = -1,
    PASS = 0,
    BID = 1,
    CAPOT = 2,
    GENERAL = 3,
    DOUBLE = 4,
} contract_type;

struct Card
{
    int color;
    int trump_value;
    int not_trump_value;
    int all_trump_value;
    int none_trump_value;
    int value;
    char display[10];
};

struct Contract
{
    contract_type type;
    int color;
    int goal_score;
    char display[30];
};

typedef struct Contract Contract;

typedef struct Card Card;

struct Player
{
    char name[20];
    Card card[8];
    Contract contract;
};

typedef struct Player Player;

struct Team
{
    Player player[2];
    int score;
};

typedef struct Team Team;

#endif