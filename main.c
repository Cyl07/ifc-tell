#include "./gui/gui.h"
#include "./function/functions.h"
#include <stdbool.h>
#include <stdio.h>

int main(void)
{
    int choice = 1;
    bool exit = false;
    while (!exit)
    {
        display("./gui/main_menu.txt", ',', choice);
        choice = keyCheck(choice, 3);
        if (choice > 50)
        {
            choice -= 50;
            switch (choice)
            {
            case 1:
                exit = true;
                new_game();
                break;
            case 2:
                score();
                break;
            case 3:
                exit = true;
                break;
            }
        }
    }
    return 0;
}